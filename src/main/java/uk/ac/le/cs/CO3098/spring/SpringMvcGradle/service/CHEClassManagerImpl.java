package uk.ac.le.cs.CO3098.spring.SpringMvcGradle.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uk.ac.le.cs.CO3098.spring.SpringMvcGradle.domain.CHEClass;
import uk.ac.le.cs.CO3098.spring.SpringMvcGradle.repository.CHEClassRepository;

import java.util.List;

@Service
public class CHEClassManagerImpl implements CHEClassManager{

    @Autowired
    CHEClassRepository cheClassRepository;

    @Override
    public void saveOrUpdate(CHEClass cheClass) {
        cheClassRepository.save(cheClass);
    }
    @Override
    public List<CHEClass> getAllCHEClasses() {
        return cheClassRepository.findAll();
    }
    @Override
    public void deleteById(int id) {
        cheClassRepository.deleteById(id);
    }
    @Override
    public CHEClass findById(int id) {
        return  cheClassRepository.findById(id).get();
    }

    @Override
    public CHEClass findByCid(int cid) {
        return null;
    }

    @Override
    public CHEClass findByName(String name) {
        return cheClassRepository.findByName(name);
    }

}
