package uk.ac.le.cs.CO3098.spring.SpringMvcGradle.controller;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import uk.ac.le.cs.CO3098.spring.SpringMvcGradle.ViewModel.CheClassViewModel;
import uk.ac.le.cs.CO3098.spring.SpringMvcGradle.domain.CHEClass;
import uk.ac.le.cs.CO3098.spring.SpringMvcGradle.domain.Parent;
import uk.ac.le.cs.CO3098.spring.SpringMvcGradle.repository.CHEClassRepository;
import uk.ac.le.cs.CO3098.spring.SpringMvcGradle.repository.ParentRepository;
import uk.ac.le.cs.CO3098.spring.SpringMvcGradle.service.CHEClassManager;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@RequestMapping(value = "/cheditor/api")
public class MainController {
    @Autowired
    private CHEClassManager cheClassManager;

    @Autowired
    private ParentRepository parentRepository;

    @Autowired
    private CHEClassRepository cheClassRepository;
    @RequestMapping(value="/")
    public ModelAndView index(){
        CheClassViewModel cheClassViewModel = new CheClassViewModel();
        ModelAndView modelAndView = new ModelAndView("create");
        modelAndView.addObject("cheClassViewModel",cheClassViewModel);
        return modelAndView;
    }



    @RequestMapping("/listAll")
    public ModelAndView listAll(){

        List<CHEClass> cheClasses = cheClassRepository.findAll();

        ModelAndView modelAndView = new ModelAndView("listAll");

        modelAndView.addObject("classList",cheClasses);

        return modelAndView;
    }


    // INSERT METHODS

    // By Form
    @RequestMapping(value = "/addClass")
    public ModelAndView addClass(@ModelAttribute CheClassViewModel cheClassViewModel){
        String result = null;
        ModelAndView modelAndView = new ModelAndView("listAll");
        List<CHEClass> cheClasses = cheClassRepository.findAll();
        modelAndView.addObject("classList",cheClasses);
        // make sure cid does not already exists
        CHEClass cheClass = cheClassRepository.getByCid(cheClassViewModel.getCid());
        if(cheClass == null) {
            cheClass = cheClassRepository.findByName(cheClassViewModel.getName());
            // make sure name does not exists
            if (cheClass == null) {
                // if parent id is provided
                if (cheClassViewModel.getPid() != null) {
                    System.out.println("Parent Id = "+cheClassViewModel.getPid());
                    cheClass = cheClassRepository.getByCid(cheClassViewModel.getPid());
                    //System.out.println(cheClass.toString());

                    if (cheClass == null) {
                        // record does not exists
                        result = "Parent Does Not Exists ";
                    } else {
                        cheClass = CheClassViewModel.ToCHEClassModel(cheClassViewModel);
                        cheClassRepository.save(cheClass);
                        cheClasses = cheClassRepository.findAll();
                        modelAndView.addObject("classList", cheClasses);
                        Parent parent = new Parent();
                        parent.setPid(cheClass.getPid());
                        parent.setCid(cheClass.getCid());
                        parentRepository.save(parent);
                        result = "Class Created Successfully! ";
                        // save both
                    }
                }
                else{
                    cheClass = CheClassViewModel.ToCHEClassModel(cheClassViewModel);
                    cheClassRepository.save(cheClass);
                    cheClasses = cheClassRepository.findAll();
                    modelAndView.addObject("classList", cheClasses);
                    result = "Class Created Successfully! ";
                }
            }
            else {
                result = "Class Name Already Exists ";
            }
        }
        else {
            result = "Class Id Already Exists ";
        }
        modelAndView.addObject("result",result);
        return modelAndView;
    }


    // By Json Request
    @RequestMapping(value="/addClassJSON")
    @ResponseBody
    public Map<String,String> addClassJSON(@RequestBody String json){
        HashMap<String,String> response = new HashMap<>();
        Gson gson = new Gson();
        CheClassViewModel cheClassViewModel = gson.fromJson(json,CheClassViewModel.class);
        response = MapAddClassJSON(cheClassViewModel,cheClassRepository,parentRepository);
        return response;

    }


    // By Json Request
    @RequestMapping(value="/addClassesJSON")
    @ResponseBody
    public List<Map<String,String>> addClassesJSON(@RequestBody String json) {

        List<Map<String,String>> responseList = new ArrayList<>();

        JSONArray jsonArray = null;
        Gson gson = new Gson();
        try {
            jsonArray = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i=0;i<jsonArray.length();i++){

            try {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                CheClassViewModel cheClassViewModel = gson.fromJson(String.valueOf(jsonObject),CheClassViewModel.class);
                responseList.add(MapAddClassJSON(cheClassViewModel,cheClassRepository,parentRepository));

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return responseList;
    }






    // SEARCH METHODS


    // By Form
    @RequestMapping("/getclass/{id}")
    @ResponseBody
    public String  getClass(@PathVariable(value="id") String id) {
        System.out.println("ID = "+id);
        CHEClass cheClass = cheClassRepository.getByCid(Integer.valueOf(id));
        Gson gson = new Gson();
        HashMap<String,String> result = new HashMap<>();
        if(cheClass != null){
            result.put("cid", String.valueOf(cheClass.getCid()));
            result.put("name",cheClass.getName());
            result.put("abstract", String.valueOf(cheClass.isAbstract()));
        }
        else{
            result.put("error","true");
            result.put("message","Cid "+id+" Does Not Exists");
        }
        return gson.toJson(result);
    }

    // By Json Request
    @RequestMapping("/getclass")
    @ResponseBody
    public String getClassJSON(@RequestBody String id){
        HashMap<String,String> result = new HashMap<>();
        Gson gson = new Gson();
        CheClassViewModel cheClassViewModel = gson.fromJson(id,CheClassViewModel.class);
        CHEClass cheClass = cheClassRepository.getByCid(cheClassViewModel.getCid());
        if(cheClass == null){
            result.put("error","true");
            result.put("message","Class Not Found");
            return gson.toJson(result);
        }
        else{
            return gson.toJson(cheClass);
        }
    }





    // DELETE METHODS


    // By Form
    @RequestMapping("/deleteclass/{id}")
    public ModelAndView deleteClass(@PathVariable(value = "id") String id){

        ModelAndView modelAndView = new ModelAndView("redirect:/listAll");

        deleteClass(Integer.parseInt(id));

        List<CHEClass> cheClasses = cheClassRepository.findAll();

        modelAndView.addObject("classList",cheClasses);

        return modelAndView;

    }

    private void deleteClass(int parseInt) {
        CHEClass cheClass = cheClassRepository.getByCid(parseInt);


        // for parent deletion
        if (cheClass.getPid() != null){
            // go to parent table
            parentRepository.deleteByCidAndPid(cheClass.getCid(),cheClass.getPid());
            cheClass.setPid(null);
            cheClassRepository.save(cheClass);
        }

        // for child

        List<CHEClass> cheClasses = cheClassRepository.getAllByPid(parseInt);

        for(CHEClass cheClass1 : cheClasses){
            parentRepository.deleteByCidAndPid(cheClass1.getCid(),cheClass1.getPid());
            cheClass1.setPid(null);
            cheClassRepository.save(cheClass1);
        }
    }

    // By Json Request
    @RequestMapping("/deleteclass")
    @ResponseBody
    public String deleteClassJSON(@RequestBody String id){

        HashMap<String,String> result = new HashMap<>();

        Gson gson = new Gson();

        CheClassViewModel cheClassViewModel = gson.fromJson(id,CheClassViewModel.class);

        deleteClass(cheClassViewModel.getCid());

        return  "Successfully Deleted ";

    }




    //GET SUPER CLASSES METHOD

    @RequestMapping("/superclasses")
    @ResponseBody
    public String getSuperClasses(@RequestBody String id) throws JSONException {

        System.out.println("Comes Here ");

        JSONObject jsonObject = new JSONObject(id);

        Integer cid = (Integer) jsonObject.get("cid");



        Parent currentNode = parentRepository.findByCid(cid);

        Parent parent = null;

        List<Parent> parentList = new ArrayList<>();

        if(currentNode != null){
            parentList.add(currentNode);
        }

        do{
            parent = parentRepository.findByCid(currentNode.getPid());
            if(parent != null){
                parentList.add(parent);
                currentNode = parent;
            }
        }
        while(parent != null);

        Gson gson = new Gson();




        System.out.println(parentList);

        List<HashMap<String,String>> mapList = new ArrayList<>();

        for(Parent node : parentList){

            CHEClass cheClass = cheClassRepository.getByCid(node.getPid());

            HashMap<String,String> hashMap = new HashMap<>();

            hashMap.put("id", String.valueOf(cheClass.getCid()));

            hashMap.put("name",cheClass.getName());

            mapList.add(hashMap);

        }


        return gson.toJson(mapList);

    }


    public static HashMap<String,String> MapAddClassJSON(CheClassViewModel cheClassViewModel, CHEClassRepository cheClassRepository, ParentRepository parentRepository){
        HashMap<String,String> response = new HashMap<>();

        if(cheClassViewModel.getCid() != null){
            if(cheClassViewModel.getName() != null){
                CHEClass cheClass = cheClassRepository.getByCid(cheClassViewModel.getCid());
                if(cheClass == null) {
                    cheClass = cheClassRepository.findByName(cheClassViewModel.getName());
                    // make sure name does not exists
                    if (cheClass == null) {
                        // if parent id is provided
                        if (cheClassViewModel.getPid() != null) {
                            cheClass = cheClassRepository.getByCid(cheClassViewModel.getPid());
                            if (cheClass == null) {
                                // record does not exists
                                response.put("ret","false");
                                response.put("message","Parent Id '"+cheClassViewModel.getPid()+"' Does Not Exists ");
                            } else {
                                cheClass = CheClassViewModel.ToCHEClassModel(cheClassViewModel);
                                cheClassRepository.save(cheClass);
                                Parent parent = new Parent();
                                parent.setPid(cheClass.getPid());
                                parent.setCid(cheClass.getCid());
                                parentRepository.save(parent);
                                response.put("ret","true");
                                // save both
                            }
                        }
                        else{
                            cheClass = CheClassViewModel.ToCHEClassModel(cheClassViewModel);
                            cheClassRepository.save(cheClass);
                            response.put("ret","true");
                        }
                    }
                    else {
                        response.put("ret","false");
                        response.put("message","Class Name '"+cheClassViewModel.getName()+"' Already Exists ");
                    }
                }
                else {
                    response.put("ret","false");
                    response.put("message","Class Id '"+cheClassViewModel.getCid()+"' Already Exists ");
                }
            }
            else{
                response.put("ret","false");
                response.put("message","Class Name Not Found ");
            }
        }
        else{
            response.put("ret","false");
            response.put("message","Class Id Not Found ");
        }

        return response;
    }





}

