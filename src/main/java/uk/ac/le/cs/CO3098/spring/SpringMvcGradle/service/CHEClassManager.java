package uk.ac.le.cs.CO3098.spring.SpringMvcGradle.service;

import uk.ac.le.cs.CO3098.spring.SpringMvcGradle.domain.CHEClass;

import java.util.List;

public interface CHEClassManager {

    public void saveOrUpdate(CHEClass cheClass);

    public List<CHEClass> getAllCHEClasses();

    public void deleteById(int id);

    public CHEClass findById(int id);

    CHEClass findByCid(int cid);

    CHEClass findByName(String name);
}
