package uk.ac.le.cs.CO3098.spring.SpringMvcGradle.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uk.ac.le.cs.CO3098.spring.SpringMvcGradle.domain.Parent;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ParentRepository extends JpaRepository<Parent,Integer> {


    Parent findByCid(Integer cid);

    Parent findByCidAndPid(Integer cid,Integer pid);

    void deleteByCidAndPid(Integer cid,Integer pid);

}
