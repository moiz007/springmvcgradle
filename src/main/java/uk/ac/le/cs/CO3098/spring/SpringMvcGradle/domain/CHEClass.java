package uk.ac.le.cs.CO3098.spring.SpringMvcGradle.domain;
import lombok.Data;
import uk.ac.le.cs.CO3098.spring.SpringMvcGradle.ViewModel.CheClassViewModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CHEClass {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    Integer cid;

    Integer pid;

    String name;

    boolean isAbstract;

    public CHEClass(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public void setAbstract(boolean anAbstract) {
        isAbstract = anAbstract;
    }

    public String toString(){
        return "CHE Class(Cid = "+cid+" Pid = "+pid+" name = "+name+" isAbstract = "+isAbstract+")";
    }
}
