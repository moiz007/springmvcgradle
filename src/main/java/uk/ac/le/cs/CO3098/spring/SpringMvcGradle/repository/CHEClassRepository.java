package uk.ac.le.cs.CO3098.spring.SpringMvcGradle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uk.ac.le.cs.CO3098.spring.SpringMvcGradle.domain.CHEClass;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface CHEClassRepository extends JpaRepository<CHEClass,Integer>{

    CHEClass getByCid(Integer cid);

    void deleteByCid(Integer cid);

    CHEClass findByName(String name);

    CHEClass findByPid(Integer pid);

    List<CHEClass> getAllByPid(Integer pid);

}
