package uk.ac.le.cs.CO3098.spring.SpringMvcGradle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcGradleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcGradleApplication.class, args);
	}

}
