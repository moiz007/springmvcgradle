package uk.ac.le.cs.CO3098.spring.SpringMvcGradle.ViewModel;

import lombok.Data;
import uk.ac.le.cs.CO3098.spring.SpringMvcGradle.domain.CHEClass;


@Data
public class CheClassViewModel {

    private int id;

    private Integer cid;

    private Integer pid;

    private String name;

    private Integer isAbstract;

    public CheClassViewModel(){

    }


    public static CHEClass ToCHEClassModel(CheClassViewModel cheClassViewModel){
        CHEClass cheClass = new CHEClass();
        cheClass.setId(cheClassViewModel.getId());
        cheClass.setCid(cheClassViewModel.getCid());
        if(cheClassViewModel.getPid() != null){
            cheClass.setPid(cheClassViewModel.getPid());
        }
        cheClass.setName(cheClassViewModel.getName());

        if(cheClassViewModel.getIsAbstract() == 0){
            cheClass.setAbstract(false);
        }
        else{
            cheClass.setAbstract(true);
        }
        return cheClass;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIsAbstract() {
        return isAbstract;
    }

    public void setIsAbstract(Integer isAbstract) {
        this.isAbstract = isAbstract;
    }
}
